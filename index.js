// alert("What's the matter? Never take a shortcut before?")

// Functions

	// function printInput(){
	// 	let nickname = prompt("Enter your nickname");
	// 	console.log("Hi " + nickname);
	// };

	// printInput();

	// Parameters and Arguments

	function printName(name){
		console.log("My name is " + name);
	};

	printName() // result: undefined
	printName("Sean");
	printName("Shaun");

	let variableName = "Shawn";
	printName(variableName);

	function checkDivisibilityBy8(num){
		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is " + remainder);

		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleBy8);
	};

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(2548966);

	// Function as Arguments

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed");
	};

	function invokeFunction(argumentFunction){
		argumentFunction();
	};

	invokeFunction(argumentFunction);

// Using multiple parameters
	
	function createFullName(firstName, middleName, lastName){
		console.log(firstName + ' ' + middleName + ' ' + lastName);
	};

	createFullName("Jane", "Dela", "Cruz");
	createFullName("Dela", "Jane", "Cruz");
	// createFullName(prompt("Enter first name"), prompt("Enter middle name"), prompt("Enter last name"))

	// using multiple variables as multiple arguments
	let firstName = "John"
	let middleName = "Reyes"
	let lastName = "Garcia"

	createFullName(firstName, middleName, lastName);

	function printFullName(middleName, firstName, lastName){
		console.log(firstName + ' ' + middleName + ' ' + lastName);
	};

	printFullName("Jan", "Asuncion", "Cruz");
	// result: Asuncion Jan Cruz

	// return statement

	function returnFullName(firstName, lastName, middleName){

		return firstName + " " + middleName  + " " + lastName
		console.log("This message should not be printed");
	}

	let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
	console.log(completeName);

	console.log(returnFullName(firstName, middleName, lastName));

	function returnAddress(city, country){

		let fullAddress = city + ', ' + country;
		return fullAddress;
	}

	let myAddress = returnAddress("Pasig City", "Philippines");
	console.log(myAddress);

	function printPlayerInfo(username, level, job){
		console.log('Username: ' + username);
		console.log('Level: ' + level);
		console.log('Job: ' + job);

		return "Username: " + username + ' ' + "Level: " + level + ' ' + "Job: " + job
	};

	let user1 = printPlayerInfo("theTinker", 95, "Warrior");
	console.log(user1);

	// parameters = name of variables in local scope
	// arguments = actual value passed on to the function